# py_redis_otel_prom_jaeg_grafana


## What it is ? 
This is a simple devops project connecting dummy api to Opentelemetry-Collector then to Prometheus and Jaeger and to Grafana for visualization.
Please see [./architecture.png](architecture.png)

## Getting started
usefull commands:
``` bash
# first time just launch podman-compose
podman-compose up

bash ./reload_podmanCompose.sh
# A simple script to stop and rm images and containers, then relaunch the podman-composer

bash ./clean_podman.sh
# will stop and rm all containers and images
``` 

### PYTHON-REDIS

#### USER-API
TODO: all

#### Redis-USER
TODO: all

#### TASK-API
TODO: all

#### Redis-TASK
TODO: all

### Prometheus
#### how to use
go to status page : 
Give it about 30 seconds to collect data
you should see the endpoint otelcollector: 
    8888 # Prometheus metrics exposed by the collector
    8889 # Prometheus exporter metrics exposed by the collector
    9090 # prometheus metrics itself for testing purposes

got to graph page : 
    add otelcol_exporter_enqueue_failed_log_records to the table and execute
    add another panel
    add otelcol_receiver_accepted_spans to the table and execute

You can also verify that Prometheus is serving metrics about itself by navigating to its own metrics endpoint: http://localhost:9090/metrics.

### Jaeger
#### how to use
go to http://localhost:16686/search
after launching a http request :http://localhost:10000/loop_test
you should see the client-api and server-api in Service. 
check out the span to retrace the request


### Grafana

#### how to use
first connection the log in is admin and password is admin
install the datasources
In the sidebar, click the Explore (compass) icon.
In the Query editor, where it says Enter a PromQL query…, 
enter otelcol_receiver_accepted_spans{} and then press Shift + Enter. 
A graph appears.

#### Install datasources manually
first connection the log in is admin and password is admin
otherwise install it : add datasource > prometheus and jaeger
url for prometheus : http://prometheus:9090
url for jaeger     : 

#### Install datasources automatically
TODO:



## Localhosts to checkout
User-Api : [http://localhost:10000/data](http://localhost:10000/loop_test)
Redis-User : yet to come
task-api : yet to come [http://localhost:5000](http://localhost:5000)
Redis-User : yet to come

Jaeger : [http://localhost:16686](http://localhost:16686/)
Prometheus : [http://localhost:9090/](http://localhost:9090/)
Grafana : [http://localhost:3000/](http://localhost:3000/)





## Authors 
enter your name :
Virginie


## Project status
Work In Progress - see DEV_LOG.MD for more info 