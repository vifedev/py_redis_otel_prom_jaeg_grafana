# DEV_LOG
A file intended to devops, useful links, commands...
What is not working, where to look up, the work todo and the one in progress


## Python Redis
### USER-API
TODO: all

### Redis-USER
TODO: all

### TASK-API
TODO: all

### Redis-TASK
TODO: all




## Grafana
TODO:
configurer les datasources en automatique ainsi que le nom d'utilisateur et login
container_name récupére les hostnames des containers. C'est ce qu'on utlise pour l'url des datasources. 
la première connexion à grafana il faut sign in avec admin/admin




## Usefull sources :
### GRAFANA
https://grafana.com/docs/grafana/latest/setup-grafana/configure-docker/
https://grafana.com/tutorials/grafana-fundamentals/?utm_source=grafana_gettingstarted
https://grafana.com/docs/grafana/latest/datasources/jaeger/
https://grafana.com/docs/grafana/latest/administration/provisioning/#datasources

### Prometheus 
- https://prometheus.io/docs/introduction/first_steps/
- https://github.com/jaegertracing/jaeger/tree/main/docker-compose/monitor
- https://opentelemetry-python-yusuket.readthedocs.io/en/latest/ext/prometheus/prometheus.html

### OTEL
https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/examples/demo

### PYTHON FLASK + REDIS
https://realpython.com/python-redis/
https://github.com/fcakyon/flask-redis-docker
https://www.youtube.com/watch?v=PPT1FElAS84
https://realpython.com/flask-by-example-implementing-a-redis-task-queue/