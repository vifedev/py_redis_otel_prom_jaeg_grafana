#!/bin/bash


podman stop $(podman ps -a -q ) 
podman rm $(podman ps -a -q )
# détruit les containers
podman rmi $(podman images -q)
# détruit toutes les images
podman images
podman ps
podman-compose up -d